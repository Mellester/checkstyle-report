FROM openjdk:8-jre-alpine
COPY target/checkstyle-report-pipe-shaded.jar /checkstyle-report-pipe.jar

ENTRYPOINT ["java", "-jar", "/checkstyle-report-pipe.jar"]
