/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import se.bjurr.violations.lib.model.SEVERITY;
import se.bjurr.violations.lib.model.Violation;
import se.bjurr.violations.lib.reports.Parser;

/**
 *
 * @author mkleint
 */
public class SeverityComparatorTest {

    public SeverityComparatorTest() {
    }

    @Test
    public void testCompare1() {
        List<Violation> lst = new ArrayList<>();
        lst.add(createViolation(SEVERITY.ERROR));
        lst.add(createViolation(SEVERITY.WARN));
        lst.add(createViolation(SEVERITY.INFO));
        SeverityComparator instance = new SeverityComparator();
        lst.sort(instance);
        assertEquals(SEVERITY.ERROR, lst.get(0).getSeverity());
        assertEquals(SEVERITY.WARN, lst.get(1).getSeverity());
        assertEquals(SEVERITY.INFO, lst.get(2).getSeverity());
    }

    private static Violation createViolation(SEVERITY severity) {
        return Violation.violationBuilder()
                .setCategory("cat")
                .setColumn(1)
                .setEndLine(1)
                .setFile("f.txt")
                .setMessage("x")
                .setReporter("r")
                .setStartLine(1)
                .setParser(Parser.CHECKSTYLE)
                .setSeverity(severity)
                .build();
    }

    @Test
    public void testCompare2() {
        List<Violation> lst = new ArrayList<>();
        lst.add(createViolation(SEVERITY.INFO));
        lst.add(createViolation(SEVERITY.WARN));
        lst.add(createViolation(SEVERITY.ERROR));
        SeverityComparator instance = new SeverityComparator();
        lst.sort(instance);
        assertEquals(SEVERITY.ERROR, lst.get(0).getSeverity());
        assertEquals(SEVERITY.WARN, lst.get(1).getSeverity());
        assertEquals(SEVERITY.INFO, lst.get(2).getSeverity());
    }

}
