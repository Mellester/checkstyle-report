#!/usr/bin/env bash
#
# Configure git to allow push back to the remote repository.
#
#

set -e

# Configure git
git config user.name "Pipelines Tasks"
git config user.email commits-noreply@bitbucket.org
git remote set-url origin "${BITBUCKET_GIT_HTTP_ORIGIN}"
